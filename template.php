<?php

/**
 * Override or insert PHPTemplate variables into the page templates.
 */
function clearlooks_preprocess_page(&$vars) {
  return $vars;
}

/**
 * Override or insert PHPTemplate variables into the node templates.
 */
function clearlooks_preprocess_node(&$vars) {
  // Set author information line separately from the full $submitted variable.
  $vars['authored'] = t('Submitted by') .' '. $vars['name'];
  return $vars;
}

/**
 * Override the search form (theme, not block) to remove the label.
 */
function clearlooks_search_theme_form($form) {
  unset($form['search_theme_form']['#title']);
  return drupal_render($form);
}
